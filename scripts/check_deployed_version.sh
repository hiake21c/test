#!/usr/bin/env bash

cd ${WORKDIR}

eval DATA_SEED='$'$TARGET'_DATA_SEED'
echo -n "${DATA_SEED}"| base64 -d > ./dataSeed.json
TARGET_BASE_URL=$(jq -r '.preDefined.baseURLDirect' dataSeed.json)

if [[ $TARGET == "DEMO" ]]; then
  echo -e "Checking DEMO Deployed Version..."
   deployed_version=$(curl -s "${TARGET_BASE_URL}/iam/version" | jq -r ".version")
   repo_version=$(jq -r ".version" version.json)
  deployed_git_hash=$(curl -s "${TARGET_BASE_URL}/iam/version" | jq -r ".gitHash")
  repo_git_hash=$(git rev-parse --verify HEAD)
  timeout=0
  while [ ${deployed_git_hash} != ${repo_git_hash} ];
  do
    deployed_git_hash=$(curl -s "${TARGET_BASE_URL}/iam/version" | jq -r ".gitHash")
    if [[ ${timeout} -le 2 ]]; then
      sleep 60
      timeout=$((timeout + 1))
      echo "Timeout : ${timeout}x"
    else
      export AUTOMATION_PIPELINE_MESSAGE="- Service Version Not Match"
      export AUTOMATION_PIPELINE_STATUS="TIMEOUT"
      echo "Version Didn't Match"
      break
    fi
  done
else
  echo -e "Checking ${TARGET} Deployed Version..."
  deployed_version=$(curl -s "${TARGET_BASE_URL}/iam/version" | jq -r ".version")
  repo_version=$(jq -r ".version" version.json)
  deployed_git_hash=$(curl -s "${TARGET_BASE_URL}/iam/version" | jq -r ".gitHash")
  repo_git_hash=$(git rev-parse --verify HEAD)
  timeout=0
  while [ ${deployed_version} != ${repo_version} -o ${deployed_git_hash} != ${repo_git_hash} ];
  do
    deployed_version=$(curl -s "${TARGET_BASE_URL}/iam/version" | jq -r ".version")
    deployed_git_hash=$(curl -s "${TARGET_BASE_URL}/iam/version" | jq -r ".gitHash")
    if [[ ${timeout} -le 2 ]]; then
      sleep 60
      timeout=$((timeout + 1))
      echo "Timeout : ${timeout}x"
    else
      export AUTOMATION_PIPELINE_MESSAGE="- Service Version Not Match"
      export AUTOMATION_PIPELINE_STATUS="TIMEOUT"
      echo "Version Didn't Match"
      break
    fi
  done
fi
echo -e "Checked!"
${BASEDIR}/scripts/helpers/export_helpers.sh -a keep -o ${CI_PROJECT_DIR}/.variables
