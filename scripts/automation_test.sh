#!/usr/bin/env bash

init(){
  readonly orig_cwd="$PWD"
  readonly script_path="${BASH_SOURCE[0]}"
  readonly script_dir="$(dirname $(readlink -f ${script_path}))"
  readonly script_name="$(basename "$script_path")"
  readonly automation_test_script_path="${WORKDIR}/tests/gatling/"
  restartedPipeline=${RESTART}
  export doRestart="no"
  export AUTOMATION_PIPELINE_URL="${CI_PIPELINE_URL}|#${CI_PIPELINE_ID}"
}

check_env_vars(){
  : "${AWS_ACCESS_KEY_ID:? AWS_ACCESS_KEY_ID must be set}"
  : "${AWS_SECRET_ACCESS_KEY:? AWS_SECRET_ACCESS_KEY must be set}"
  : "${ECR_URI:? ECR_URI must be set}"
  : "${SERVICE_NAME:? SERVICE_NAME must be set}"
  : "${TARGET:? TARGET must be set}"
}

run_automation_test(){
  cd ${WORKDIR}
  repo_version=$(jq -r ".version" version.json)
  cd ${automation_test_script_path}

  eval DATA_SEED='$'$TARGET'_DATA_SEED'
  echo -n "${DATA_SEED}"| base64 -d > ./dataSeed.json

  repo_git_hash=$(git rev-parse --verify HEAD)

  aws ecr get-login-password --region us-west-2 | docker login --username AWS --password-stdin ${ECR_URI}
  imageName=$(echo -e "${SERVICE_NAME}-gatling-test")
  imageTag=$(echo -e "134326769038.dkr.ecr.us-west-2.amazonaws.com/${imageName}:${repo_version}-${repo_git_hash}")
  isExists=$(docker pull ${imageTag} > /dev/null && echo "yes" || echo "no")

  if [[ $isExists == "yes" ]]; then
    docker tag ${imageTag} ${imageName}:latest
    mkdir -p .m2
    mkdir target
    make test-gitlab
  else
    mkdir -p .m2
    mkdir target
    make build
    docker tag ${imageName} ${imageTag}
    docker push ${imageTag}
    make test-gitlab
  fi
}

generate_automation_test_report(){
  export AUTOMATION_TEST_REPORT_URL="http://report.accelbyte.io/automation/${TARGET}/${SERVICE_NAME}-gatling-test/${CI_JOB_ID}/index.html"
    case ${CI_PIPELINE_SOURCE} in
    cross_project_pipeline|parent_pipeline|pipeline)
      export AUTOMATION_TEST_TRIGGER="Service Deployment"
    ;;

    schedule)
      export AUTOMATION_TEST_TRIGGER="Automation Scheduler"
    ;;

    web|webide|api|external|push)
      export AUTOMATION_TEST_TRIGGER="Someone"
    ;;

    *)
      export AUTOMATION_TEST_TRIGGER="Unknown"
    ;;
  esac

  if [ "${AUTOMATION_TEST_TRIGGER}" == "Automation Scheduler" ]; then
    cd ../../../
    bash ${script_dir}/automation-report.sh -s ${SERVICE_NAME} -n ${restartedPipeline} -v ${repo_version} -j ${AUTOMATION_PIPELINE_URL} -r ${AUTOMATION_TEST_REPORT_URL} -t ${AUTOMATION_TEST_TRIGGER} -c ${COMMIT_USER}
  fi

  cd ${automation_test_script_path}/target
  mv *.log report
  aws s3 cp report s3://report.accelbyte.io/automation/${TARGET}/${SERVICE_NAME}-gatling-test/${CI_JOB_ID}/ --recursive
  echo "Report test : ${AUTOMATION_TEST_REPORT_URL}"

  cd ../../../../
  if  [ -f testFailCausedBy500 ] && [ ${restartedPipeline} != "true" ] && [ "${AUTOMATION_TEST_TRIGGER}" == "Automation Scheduler" ]; then
      echo "Do Restart"
      curl --request POST --form token=$CI_JOB_TOKEN --form ref=master --form "variables[restart]=true" --form "variables[TARGET]=${TARGET}" 'https://gitlab.com/api/v4/projects/21122540/trigger/pipeline'
      doRestart="yes"
  fi

  isHtmlExist=$(grep -F .html ${automation_test_script_path}/target/report/index.html)
  if [ ! -f ${automation_test_script_path}/target/report/index.html ] || [ ! -z "$isHtmlExist" ]; then
    export AUTOMATION_PIPELINE_MESSAGE="- Some Test Failed"
    export AUTOMATION_PIPELINE_STATUS="FAILED"
    export_helpers
  else
    export AUTOMATION_PIPELINE_MESSAGE="- All Test Passed"
    export AUTOMATION_PIPELINE_STATUS="SUCCESS"
    export_helpers
  fi
}

export_helpers(){
  bash ${script_dir}/helpers/export_helpers.sh -a keep -o ${CI_PROJECT_DIR}/.variables
}

main(){
  init
  echo -e "Do Automation Testing..."
  selector=$(echo -e "${AUTOMATION_PIPELINE_STATUS}" | wc -w)
  if [[ ${selector} -ne 0 ]]; then
    echo -e "Skipped!"
    bash ${BASEDIR}/scripts/automation_slack_notifier.sh "${AUTOMATION_PIPELINE_STATUS}"
    exit 1
  else
    check_env_vars
    run_automation_test
    generate_automation_test_report
    if [[ ${doRestart} == 'no' ]]; then
      bash ${BASEDIR}/scripts/automation_slack_notifier.sh "${AUTOMATION_PIPELINE_STATUS}"
    fi
    echo -e "Done!"
  fi
}

main "$@"
