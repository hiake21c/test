#!/bin/bash
# Copyright (c) 2020 AccelByte Inc. All Rights Reserved.
# This is licensed software from AccelByte Inc, for limitations
# and restrictions contact your company contract manager.

function main() {
  while getopts "s:v:j:r:t:c:n:" option; do
    case ${option} in
    s)
      serviceName=$OPTARG
      ;;
    v)
      serviceVersion=$OPTARG
      ;;
    j)
      jobURL=$OPTARG
      ;;
    r)
      reportURL=$OPTARG
      ;;
    t)
      triggerBy=$OPTARG
      ;;
    c)
      commitBy=$OPTARG
      ;;
    n)
      if [ $OPTARG == "true" ]; then
        restart=true
      else
        restart=false
      fi
      ;;
    esac
  done
  if (grep -q "but actually found 5" ./${serviceName}/tests/gatling/target/test-apigw-gatling.log || grep -q "but actually found 5" ./${serviceName}/tests/gatling/target/test-direct-gatling.log) && ! ${restart}; then
    echo "true" >testFailCausedBy500
    echo "Restarting Test"
  else
    timestamp=$(date +%s)
    dateTime=$(date +%-m/%d/%Y)
    dateTimeNextDay=$(date +%-m/%d/%Y -d "+1 days")

    totalTestFail=0
    totalTestPass=0

    for file in $(find ./${serviceName}/tests/gatling/target/report -not -path '*/\.*' -type f | grep log); do
      fileName=$(echo $file | awk -F'/' '{print $NF}' | cut -f 1 -d '.')
      if grep -q ": KO " "${file}"; then
        echo "FAILED : "${fileName}
        totalTestFail=$((totalTestFail + 1))
      else
        echo "SUCCESS : "${fileName}
        totalTestPass=$((totalTestPass + 1))
      fi
    done
    totalTest=$((totalTestPass + totalTestFail))

    if grep -q "find.is" ./${serviceName}/tests/gatling/target/test-apigw-gatling.log || grep -q "find.is" ./${serviceName}/tests/gatling/target/test-direct-gatling.log; then
      for file in $(find ./${serviceName}/tests/gatling/target/report -not -path '*/\.*' -type f | grep html); do
        echo $(basename ${file}) >>failedTestList
      done
      result="Fail"
    else
      result="Pass"
    fi

    year=$(date +%Y)
    month=$(date +%-m)

    mkdir ${serviceName}/${year}

    # download s3 data
    rm serviceDetail.json ${serviceName}/ticketList.json ${serviceName}/${year}/${month}.json
    aws s3 cp s3://report.accelbyte.io/quality/automation-metrics/serviceDetail.json serviceDetail.json
    aws s3 cp s3://report.accelbyte.io/quality/automation-metrics/${serviceName}/${year}/${month}.json ${serviceName}/${year}/${month}.json

    if test -f "${serviceName}/${year}/${month}.json"; then
      echo "${serviceName}/${year}/${month}.json already exist"
    else
      echo "creating file ${serviceName}/${year}/${month}.json"
      echo "[]" >${serviceName}/${year}/${month}.json
    fi

    index=$(jq --arg serviceName "${serviceName}" 'map(.repository == $serviceName) | index(true)' serviceDetail.json)
    echo "index: ${index}"
    jq --arg serviceName "${serviceName}" '.[] | select(.repository == $serviceName)' serviceDetail.json >detailTmp.json

    if grep -q "startDate" detailTmp.json; then
      jq --argjson index "${index}" \
        --arg endDate "${dateTimeNextDay}" \
        '.[$index] += {endDate: $endDate}' serviceDetail.json >detailTmp.json
      cat detailTmp.json >serviceDetail.json
    else
      jq --argjson index "${index}" \
        --arg startDate "${dateTime}" \
        --arg endDate "${dateTimeNextDay}" \
        '.[$index] += {startDate: $startDate, endDate: $endDate}' serviceDetail.json >detailTmp.json
      cat detailTmp.json >serviceDetail.json
    fi

    cat failedTestList
    sed -i '/index.html/d' failedTestList
    failedTestList=""
    while read failedTest; do
      failedTestList="${failedTestList},\"${failedTest}\""
    done <failedTestList

    jq --arg timestamp "${timestamp}" \
      --arg serviceVersion "${serviceVersion}" \
      --arg serviceName "${serviceName}" \
      --arg jobURL "${jobURL}" \
      --arg reportURL "${reportURL}" \
      --arg triggerBy "${triggerBy}" \
      --arg result "${result}" \
      --argjson failedTest "[${failedTestList:1}]" \
      --arg totalTest "${totalTest}" \
      --arg totalTestFail "${totalTestFail}" \
      --arg environment "${TARGET}" \
      --arg commitURL "${COMMIT_URL}" \
      '. += [
        {
          timestamp: $timestamp,
          serviceVersion: $serviceVersion,
          serviceName: $serviceName,
          jobURL: $jobURL,
          reportURL: $reportURL,
          triggerBy: $triggerBy,
          commitURL: $commitURL,
          environment: $environment,
          failedTest: $failedTest,
          totalTest: $totalTest,
          totalTestFail: $totalTestFail,
          result: $result
        }
      ]' ${serviceName}/${year}/${month}.json >tmp.json

    cat tmp.json >${serviceName}/${year}/${month}.json

    aws s3 cp ${serviceName}/${year}/${month}.json s3://report.accelbyte.io/quality/automation-metrics/${serviceName}/${year}/${month}.json
    aws s3 cp serviceDetail.json s3://report.accelbyte.io/quality/automation-metrics/serviceDetail.json
  fi
}

main "$@"
