#!/usr/bin/env bash

if [[ ! -z ${PIPELINE_DIR}/${CI_JOB_ID}/${CI_PIPELINE_ID} ]]; then
    mkdir -p ${PIPELINE_DIR}/${CI_JOB_ID}/${CI_PIPELINE_ID}
    cp -r /tmp/${SERVICE_NAME} ${PIPELINE_DIR}/${CI_JOB_ID}/${CI_PIPELINE_ID}/  > /dev/null 2>&1
fi
cd ${WORKDIR}
touch ${WORKDIR}/.variables
touch ${CI_PROJECT_DIR}/.variables

if [[ $TARGET == "DEMO" && ${BB_BRANCH} == "master" ]]; then
    deployed_version=$(curl -s "https://demo.accelbyte.io/iam/version" | jq -r ".version")
    if  echo "${deployed_version}" | grep -q "hotfix"; then
    listOfBranch=$(git branch -a | grep release | tr -d ' ')
    while IFS= read -r branch; do
        gitHash=$(git log -n 1 --pretty=format:'%H' $branch)
        gitHashDemo=$(curl -s "https://demo.accelbyte.io/iam/version" | jq -r ".gitHash")
        if [[ "$gitHash" == "$gitHashDemo" ]]; then
            export BB_BRANCH=$branch
        fi
    done <<< "$listOfBranch"
    else
        latest_tag=$(git describe --tags --abbrev=0)
        export BB_BRANCH=$(git rev-list -n 1 ${latest_tag})
    fi
fi

eval BRANCH='$'$TARGET'_BB_BRANCH'

if  [[ ! -z ${BRANCH} ]] ; then
    export BB_BRANCH=${BRANCH}
fi

git checkout ${BB_BRANCH} -f
export COMMIT_HASH=$(git log -n 1 --pretty=format:'%H')
export GIT_HASH=${COMMIT_HASH}
export REVISION_ID="$(cat ${WORKDIR}/version.json | jq -r .version)"
export SLACK_EMAIL_BASE64=$(git show -s --format='%ae' `git log --pretty=format:'%h' -n 1` | base64 -w0)
export COMMIT_MESSAGE_BASE64=$(git show -s --format=%s%b `git log --pretty=format:'%h' -n 1` | base64 -w0)
export COMMIT_USER=$(git show -s --format='%ae' `git log --pretty=format:'%h' -n 1`)
export COMMIT_URL="${BB_REPO_URL}/commits/${GIT_HASH}"

# a way to determine if this is a build with a git commit hash or not
git_describe_output=$(git describe)
IFS='-' read -r -a git_describe_list <<< "$git_describe_output"
githash_exist=${git_describe_list[2]}

if [[ ! -z ${githash_exist} ]]; then
    # short form of hash, and discard the first letter, as it serve as scm identifier
    truncated_githash=${githash_exist:1}
    export VERSION="${REVISION_ID}-${truncated_githash}"
else
    export VERSION="${REVISION_ID}"
fi

${BASEDIR}/scripts/helpers/export_helpers.sh -a keep -o ${CI_PROJECT_DIR}/.variables
