#!/usr/bin/env bash

init(){
  readonly orig_cwd="$PWD"
  readonly script_path="${BASH_SOURCE[0]}"
  readonly script_dir="$(dirname $(readlink -f ${script_path}))"
  readonly script_name="$(basename "$script_path")"
  config_file=${script_dir}/export_helpers.conf
}

usage() {
  cat << EOF
Usage:
./export_helpers.sh -a <keep/generate> -c <config_file> -i <input_file> -o <output_file>
::  -a    keep/generate
          keep > save current environment variable to output file
          generate >  generate a gitlab env friendly artifact
                      to declare environment variable for other job
::  -c    Config File (optional in keep/generate action, default ./export_helpers.conf)
          > list of string for removing unnecessary environment variable
            before generate the friendly env artifact
::  -i    Input File (required in generate action)
          generate > file to be filtered before generate the friendly env artifact
::  -o    Output File (required in keep/generate action)
          the friendly env artifact file to be created
::  -h    Displays this help
EOF
  exit 0
}

remove_unimportant_env(){
  touch ${3}
  unimportant_env_selector=$(cat "${1}" | tr '\n' '|')
  output_temp=$(mktemp)
  cat ${2} > ${output_temp}
  cat ${output_temp} | awk "!/${unimportant_env_selector}/" | sed '/^"$/d' | sed 's/declare -x/export/g' > ${3}
}

keep_env(){
  keep_env_temp=$(mktemp)
  cat ${2} > ${keep_env_temp}
  export >> ${keep_env_temp}
  cat ${keep_env_temp} | sort | uniq > ${2}
  remove_unimportant_env "${1}" "${2}" "${2}"
}

cleanup(){
  rm -rf ${keep_env_temp}
}

main(){
  init
  while getopts a:c:i:o:h option
  do
    case "${option}" in
      a) action=${OPTARG};;
      c) config_file=${OPTARG};;
      i) input_file=${OPTARG};;
      o) output_file=${OPTARG};;
      h) usage;;
    esac
  done

  case ${action} in
    keep)
      if [[ -z ${output_file} ]]; then
        usage
        exit 1
      fi
      keep_env "${config_file}" "${output_file}"
    ;;
    generate)
      if [[ -z ${input_file} ]] || [[ -z ${output_file} ]]; then
        usage
        exit 1
      fi
      remove_unimportant_env "${config_file}" "${input_file}" "${output_file}"
    ;;
    *)
      usage
      exit 0
    ;;
  esac
  cleanup
}

main "$@"
