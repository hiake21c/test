#!/usr/bin/env bash

init(){
  readonly orig_cwd="$PWD"
  readonly script_path="${BASH_SOURCE[0]}"
  readonly script_dir="$(dirname $(readlink -f ${script_path}))"
  readonly script_name="$(basename "$script_path")"

  slack_message_template=${script_dir}/templates/slack_message.json
  slack_message_file=$(mktemp)

  eval CHANNEL_NAME='$'$TARGET'_SLACK_AUTOMATION_CHANNEL_NAME'
  export SLACK_CHANNEL_NAME="${CHANNEL_NAME}"

  if [[ ${TESTING,,} == "true" ]]; then
    export SLACK_CHANNEL_NAME="sdet-pipeline-test"
  fi
}

check_env_vars(){
  : "${SLACK_CHANNEL_NAME:? SLACK_CHANNEL_NAME must be set}"
  : "${AUTOMATION_SLACK_TOKEN:? AUTOMATION_SLACK_TOKEN must be set}"
  : "${COMMIT_MESSAGE_BASE64:? COMMIT_MESSAGE_BASE64 must be set}"
}

get_slack_channel_id_by_name(){
  export SLACK_CHANNEL_ID=$( \
    curl -s -X POST \
      -H "Authorization: Bearer ${AUTOMATION_SLACK_TOKEN}" \
      -F "exclude_archived=true" \
      -F "limit=1000" \
      -F "types=public_channel,private_channel" \
      https://slack.com/api/conversations.list \
      | jq -r '.channels[] | select(.name == "'"${1}"'") | .id' \
  )
}

get_slack_username() {
  slack_username_temp=$(echo "${1}" | base64 -d)
  export SLACK_USERNAME=$(\
    curl -s -X GET \
      "https://slack.com/api/users.lookupByEmail?token=${AUTOMATION_SLACK_TOKEN}&email=${slack_username_temp}" \
      | jq -r ".user.name" \
    )
}

generate_slack_message(){
  export AUTOMATION_PROJECT="<${CI_PROJECT_URL}|${CI_PROJECT_NAME}>"
  export AUTOMATION_PIPELINE_URL="<${CI_PIPELINE_URL}|#${CI_PIPELINE_ID}>"

  case ${CI_PIPELINE_SOURCE} in
    cross_project_pipeline|parent_pipeline|pipeline)
      export AUTOMATION_TEST_TRIGGER="Service Deployment"
    ;;

    schedule)
      export AUTOMATION_TEST_TRIGGER="Automation Scheduler"
    ;;

    web|webide|api|external|push)
      export AUTOMATION_TEST_TRIGGER="Someone"
    ;;

    *)
      export AUTOMATION_TEST_TRIGGER="Unknown"
    ;;
  esac

  echo -e "Pipeline Status: ${AUTOMATION_PIPELINE_STATUS}"
  case ${1} in
    success|SUCCESS|good|GOOD|green|GREEN)
      export SLACK_ATTACHMENTS_COLOR="#2eb886"
      export AUTOMATION_PIPELINE_STATUS="SUCCESS"
      export AUTOMATION_TEST_REPORT_URL="<${AUTOMATION_TEST_REPORT_URL}|#${CI_JOB_ID}>"
      export EMOJI=":heavy_check_mark:"
    ;;

    aborted|ABORTED|timeout|TIMEOUT|warning|WARNING|yellow|YELLOW)
      export SLACK_ATTACHMENTS_COLOR="#daa038"
      export AUTOMATION_PIPELINE_STATUS="ABORTED"
      export AUTOMATION_TEST_REPORT_URL="null"
      export EMOJI=":warning:"
    ;;

    failed|FAILED|danger|DANGER|red|RED)
      export SLACK_ATTACHMENTS_COLOR="#a30200"
      export AUTOMATION_PIPELINE_STATUS="FAILED"
      export AUTOMATION_TEST_REPORT_URL="<${AUTOMATION_TEST_REPORT_URL}|#${CI_JOB_ID}>"
      export EMOJI=":x:"
    ;;

    *)
      export SLACK_ATTACHMENTS_COLOR="#a30200"
      export AUTOMATION_PIPELINE_STATUS="UNCLEAR"
      export AUTOMATION_TEST_REPORT_URL="null"
      export EMOJI=":question:"
    ;;
  esac

  export AUTOMATION_COMMIT_MESSAGE=$( \
    echo -e "${COMMIT_MESSAGE_BASE64}" \
    | base64 -d -w0 | sed -E ':a;N;$!ba;s/\r{0,1}\n/\\n/g' \
    | sed 's/["]//g' \
    | sed -e "s/Approved.*//g"
  )
  echo -e "${AUTOMATION_COMMIT_MESSAGE}"
  cat ${slack_message_template} | envsubst > ${slack_message_file}
}

send_slack_notifier(){
  curl -s -X POST \
    -H "Authorization: Bearer ${AUTOMATION_SLACK_TOKEN}" \
    -H "Content-Type: application/json; charset=UTF-8" \
    -d "@${slack_message_file}" \
    https://slack.com/api/chat.postMessage \
    | jq -r "."
}

cleanup(){
  rm -rf ${slack_message_file}
}

main(){
  init
  check_env_vars
  get_slack_channel_id_by_name "${SLACK_CHANNEL_NAME}"
  echo -e "Slack Email : ${SLACK_EMAIL_BASE64}"
  get_slack_username "${SLACK_EMAIL_BASE64}"
  generate_slack_message "${1}"
  send_slack_notifier
  cleanup
}

main "$@"
