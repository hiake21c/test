#!/usr/bin/env bash

function init() {
  # sanity check
  : "${SERVICE_NAME:?SERVICE_NAME must be set}"
  : "${CI_JOB_ID:?CI_JOB_ID must be set}"

  # prepare ssh to add bitbucket.org to the known hosts
  if [[ ! -n "$(grep "^bitbucket.org " ~/.ssh/known_hosts)" ]]; then
    ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts 2>/dev/null
  fi

  #check if TARGET is on different repo
  eval REPO_URL='$'$TARGET'_BB_REPO_URL'

  if [[ ! -z ${REPO_URL} ]];then
    export BB_REPO_URL=${REPO_URL}
  fi

  # useful paths
  readonly script_path="${BASH_SOURCE[0]}"
  readonly script_dir="$(dirname $(readlink -f ${script_path}))"
  # readonly repo_dir="${SERVICE_NAME}-${CI_JOB_ID}"
  readonly tmp_dir=/tmp
  readonly cache_dir=${tmp_dir}/${SERVICE_NAME}
  readonly repo_url_ssh=$(echo ${BB_REPO_URL} | sed s_https://_git@_g | sed s_/_:_ | sed s_\$_\.git_)
}

function webhook_triggered() {
  : "${BB_REPO_URL:?BB_REPO_URL must be set}"
  : "${BB_BRANCH:?BB_BRANCH must be set}"

  # check if the repo already cloned
  if [[ -d ${cache_dir} ]]; then
    # repo exist
    cd ${cache_dir}
    git checkout master
    git pull
    # prevent local branch ahead of origin if any
    git reset --hard origin/master
    # Error handling for concurrent pull
    if [ $? -ne 0 ]; then
      sleep 1
      webhook_triggered
    fi
  else
    cd ${tmp_dir}
    git clone ${repo_url_ssh} ${SERVICE_NAME}
  fi
}

function manual_triggered() {
  : "${BB_REPO_URL:?BB_REPO_URL must be set}"
  : "${BB_BRANCH:?BB_BRANCH must be set}"

  # check if the repo already cloned
  if [[ -d ${cache_dir} ]]; then
    # repo exist
    cd ${cache_dir}
    git checkout master
    git pull
    # prevent local branch ahead of origin if any
    git reset --hard origin/master
    # Error handling for concurrent pull
    if [ $? -ne 0 ]; then
      sleep 1
      manual_triggered
    fi
  else
    cd ${tmp_dir}
    git clone ${repo_url_ssh} ${SERVICE_NAME}
  fi
}

function main() {
  init
  if [[ ${CI_PIPELINE_TRIGGERED} == "true" ]] && [[ ${DASHBOARD_TRIGGERED} != "true" ]]; then
    webhook_triggered
  else
    manual_triggered
  fi
}

main $@
